FROM ubuntu:20.04

# Next line is needed when installing time zone data
ARG DEBIAN_FRONTEND=noninteractive


RUN apt-get update \
    && apt-get install -y software-properties-common \
    && add-apt-repository ppa:npalix/coccinelle \
    && apt-get install -yq \
        bc \
        bison \
        build-essential \
        cmake \
        gcovr \
        git \
        flex \
        libfuse-dev \
        libssl-dev \
        libgtest-dev \
        curl \
        gnupg2 \
        gettext \
        pkg-config \
        wget \
        coccinelle \
        drbd-utils \
        parted

RUN apt-get install -yq \
        llvm-dev \
        libclang-dev \
        clang \
        libelf-dev \
        pkg-config \
        musl-tools \
        jq \
        curl \
        wget


RUN apt-get install -y \
    libdevmapper-dev \
    libncurses-dev \
    libuuid1 \
    libblkid1 \
    e2fslibs \
    uuid-dev \
    e2fsprogs \
    util-linux \
    libtool \
    gperf \
    rsync \ 
    texinfo \
    autopoint \
    git-merge-changelog


RUN git clone -b v3.2.153 https://git.savannah.gnu.org/git/parted.git
WORKDIR parted
RUN ./bootstrap && ./configure --enable-static --enable-static=yes --disable-shared --disable-rpath --without-readline && make

