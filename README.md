# nitro-parted-builder

`parted` requires lots of shared libraries, so we've compiled our own version of parted that requires less shared libraries and statically links some of the libraries.

To compile, run `docker build -t parted-builder .`
Copy the file `/parted/parted/parted` from the container, and you're good to go.